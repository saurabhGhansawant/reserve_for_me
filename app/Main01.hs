{-# LANGUAGE OverloadedStrings #-}
module Main where

import Web.Scotty
import Data.Aeson (FromJSON, ToJSON, decode, encode)
import Data.Text.Lazy (Text, pack)
import Control.Monad.Trans (liftIO)

data Facility = Facility { ... } deriving (Show, Generic, ToJSON, FromJSON)
data Group = Group { ... } deriving (Show, Generic, ToJSON, FromJSON)
data Booking = Booking { ... } deriving (Show, Generic, ToJSON, FromJSON)
data DateDuration = DateDuration { ... } deriving (Show, Generic, ToJSON, FromJSON)
data SearchQuery = SearchQuery { ... } deriving (Show, Generic, ToJSON, FromJSON)

main :: IO ()
main = scotty 3000 $ do
    post "/admin/add_facility" $ do
        facility <- jsonData :: ActionM Facility
        text "Facility added"

    put "/admin/update_facility/:facility_id" $ do
        facilityId <- param "facility_id"
        facility <- jsonData :: ActionM Facility
        text $ "Facility " <> facilityId <> " updated"

    delete "/admin/delete_facility/:facility_id" $ do
        facilityId <- param "facility_id"
        text $ "Facility " <> facilityId <> " deleted"

    post "/admin/create_group" $ do
        group <- jsonData :: ActionM Group
        text "Group created"

    put "/admin/update_group/add_facility/:facility_id" $ do
        facilityId <- param "facility_id"
        text $ "Facility " <> facilityId <> " added to group"

    put "/admin/update_group/remove_facility/:facility_id" $ do
        facilityId <- param "facility_id"
        text $ "Facility " <> facilityId <> " removed from group"

    delete "/admin/delete_group/:group_id" $ do
        groupId <- param "group_id"
        text $ "Group " <> groupId <> " deleted"

    post "/admin/set_holiday/facility/:facility_id" $ do
        facilityId <- param "facility_id"
        dateDuration <- jsonData :: ActionM DateDuration
        text $ "Holiday set for facility " <> facilityId

    post "/admin/set_holiday/group/:group_id" $ do
        groupId <- param "group_id"
        dateDuration <- jsonData :: ActionM DateDuration
        text $ "Holiday set for group " <> groupId

    delete "/admin/remove_holiday/:holiday_id" $ do
        holidayId <- param "holiday_id"
        text $ "Holiday " <> holidayId <> " removed"

    put "/admin/group" $ do
        facility <- jsonData :: ActionM Facility
        text "Group facilities mass updated"

    get "/facilities" $ do
        text "Fetching all facilities"

    get "/facility/:facility_id" $ do
        facilityId <- param "facility_id"
        text $ "Fetching facility " <> facilityId

    post "/user/book_facility/:facility_id" $ do
        facilityId <- param "facility_id"
        booking <- jsonData :: ActionM Booking
        text $ "Booking created for facility " <> facilityId

    post "/user/cancel_booking/:booking_id" $ do
        bookingId <- param "booking_id"
        text $ "Booking " <> bookingId <> " cancelled"

    get "/user/bookings" $ do
        text "Fetching all bookings"

    get "/user/booking/:booking_id" $ do
        bookingId <- param "booking_id"
        text $ "Fetching booking " <> bookingId

    get "/user/booking/:booking_id/status" $ do
        bookingId <- param "booking_id"
        text $ "Fetching booking status for " <> bookingId

    post "/user/booking/activate" $ do
        text "Booking activated"

    get "/search_facility" $ do
        text "Searching facilities"

    get "/search_facility_by_time" $ do
        text "Searching facilities by time"

    post "/user/add_facility_rating/:facility_id" $ do
        facilityId <- param "facility_id"
        text $ "Rating added for facility " <> facilityId

    get "/facility_ratings/:facility_id" $ do
        facilityId <- param "facility_id"
        text $ "Fetching ratings for facility " <> facilityId

    get "/admin/facility/export/:facility_id/:file_type" $ do
        facilityId <- param "facility_id"
        fileType <- param "file_type"
        text $ "Exporting facility " <> facilityId <> " as " <> fileType

