{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Web.Scotty
import Data.Aeson (FromJSON, ToJSON)
import GHC.Generics (Generic)
import Data.Text.Lazy (Text)

-- Define the Facility data type
data Facility = Facility
  { facilityId :: Int
  , facilityName :: String
  } deriving (Show, Generic, ToJSON, FromJSON)

-- Define the Group data type
data Group = Group
  { groupId :: Int
  , groupName :: String
  } deriving (Show, Generic, ToJSON, FromJSON)

-- Define the Booking data type
data Booking = Booking
  { bookingId :: Int
  , bookingFacilityId :: Int
  , bookingUserId :: Int
  , bookingTime :: String
  } deriving (Show, Generic, ToJSON, FromJSON)

-- Define the DateDuration data type
data DateDuration = DateDuration
  { startDate :: String
  , endDate :: String
  } deriving (Show, Generic, ToJSON, FromJSON)

-- Define the SearchQuery data type
data SearchQuery = SearchQuery
  { query :: String
  } deriving (Show, Generic, ToJSON, FromJSON)

main :: IO ()
main = scotty 3000 $ do
  -- Endpoint to add a facility
  post "/admin/add_facility" $ do
    facility <- jsonData :: ActionM Facility
    text "Facility added"

  -- Endpoint to update a facility
  put "/admin/update_facility/:facility_id" $ do
    facilityId <- param "facility_id"
    facility <- jsonData :: ActionM Facility
    text $ "Facility " <> facilityId <> " updated"

  -- Endpoint to delete a facility
  delete "/admin/delete_facility/:facility_id" $ do
    facilityId <- param "facility_id"
    text $ "Facility " <> facilityId <> " deleted"

  -- Endpoint to create a group
  post "/admin/create_group" $ do
    group <- jsonData :: ActionM Group
    text "Group created"

  -- Endpoint to add a facility to a group
  put "/admin/update_group/add_facility/:facility_id" $ do
    facilityId <- param "facility_id"
    text $ "Facility " <> facilityId <> " added to group"

  -- Endpoint to remove a facility from a group
  put "/admin/update_group/remove_facility/:facility_id" $ do
    facilityId <- param "facility_id"
    text $ "Facility " <> facilityId <> " removed from group"

  -- Endpoint to delete a group
  delete "/admin/delete_group/:group_id" $ do
    groupId <- param "group_id"
    text $ "Group " <> groupId <> " deleted"

  -- Endpoint to set a holiday for a facility
  post "/admin/set_holiday/facility/:facility_id" $ do
    facilityId <- param "facility_id"
    dateDuration <- jsonData :: ActionM DateDuration
    text $ "Holiday set for facility " <> facilityId

  -- Endpoint to set a holiday for a group
  post "/admin/set_holiday/group/:group_id" $ do
    groupId <- param "group_id"
    dateDuration <- jsonData :: ActionM DateDuration
    text $ "Holiday set for group " <> groupId

  -- Endpoint to remove a holiday
  delete "/admin/remove_holiday/:holiday_id" $ do
    holidayId <- param "holiday_id"
    text $ "Holiday " <> holidayId <> " removed"

  -- Endpoint to mass update group facilities
  put "/admin/group" $ do
    facility <- jsonData :: ActionM Facility
    text "Group facilities mass updated"

  -- Endpoint to fetch all facilities
  get "/facilities" $ do
    text "Fetching all facilities"

  -- Endpoint to fetch a specific facility
  get "/facility/:facility_id" $ do
    facilityId <- param "facility_id"
    text $ "Fetching facility " <> facilityId

  -- Endpoint to create a booking for a facility
  post "/user/book_facility/:facility_id" $ do
    facilityId <- param "facility_id"
    booking <- jsonData :: ActionM Booking
    text $ "Booking created for facility " <> facilityId

  -- Endpoint to cancel a booking
  post "/user/cancel_booking/:booking_id" $ do
    bookingId <- param "booking_id"
    text $ "Booking " <> bookingId <> " cancelled"

  -- Endpoint to fetch all bookings
  get "/user/bookings" $ do
    text "Fetching all bookings"

  -- Endpoint to fetch a specific booking
  get "/user/booking/:booking_id" $ do
    bookingId <- param "booking_id"
    text $ "Fetching booking " <> bookingId

  -- Endpoint to fetch the status of a specific booking
  get "/user/booking/:booking_id/status" $ do
    bookingId <- param "booking_id"
    text $ "Fetching booking status for " <> bookingId

  -- Endpoint to activate a booking
  post "/user/booking/activate" $ do
    text "Booking activated"

  -- Endpoint to search facilities
  get "/search_facility" $ do
    text "Searching facilities"

  -- Endpoint to search facilities by time
  get "/search_facility_by_time" $ do
    text "Searching facilities by time"

  -- Endpoint to add a rating for a facility
  post "/user/add_facility_rating/:facility_id" $ do
    facilityId <- param "facility_id"
    text $ "Rating added for facility " <> facilityId

  -- Endpoint to fetch ratings for a facility
  get "/facility_ratings/:facility_id" $ do
    facilityId <- param "facility_id"
    text $ "Fetching ratings for facility " <> facilityId

  -- Endpoint to export facility data
  get "/admin/facility/export/:facility_id/:file_type" $ do
    facilityId <- param "facility_id"
    fileType <- param "file_type"
    text $ "Exporting facility " <> facilityId <> " as " <> fileType

